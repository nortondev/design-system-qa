package wwnorton_objectfactory;


	import java.util.List;

	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.PageFactory;
	import org.openqa.selenium.support.ui.Select;

	public class Component_Dropdown {
		
	WebDriver driver;

		public Component_Dropdown(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}


		//Main Dropdown
		@FindBy(id="dropdown")
		WebElement mainDropdown;
		
		//Default Dropdown
		@FindBy(id="dropdown--default")
		WebElement DefaultDropDown;
		
		//MatchListBoxWidth DD
		@FindBy(id="dropdown--match-listbox-width")
		WebElement MatchListBoxWidth;
		
		@FindBy(id="dropdown--match-button-width")
		WebElement MatchButtonWidth;
		
		//FullyControlled DD
		@FindBy(id="dropdown--fully-controlled")
		WebElement FullyControlled;
		
		//Button Width DD
		@FindBy(id="dropdown--match-button-width")
		WebElement ListboxWidth;
		
		//ComplexOption DD
		@FindBy(id="dropdown--complex-options")
		WebElement ComplexOption;
		
		@FindBy(id="dropdown--different-children-types")
		WebElement DifferentChildrenType;
			
		//Generic Page Frame
		@FindBy(id="storybook-preview-iframe")
		WebElement Frame;
			
		@FindBy(xpath = "//button[@class='nds-button--outline nds-button nds-dropdown__button']")
		WebElement ChooseElement;
		
		@FindBy(xpath = "//span[@class='nds-dropdown__option-label']") //Working for complex and default also
		List<WebElement> dropDown_ListAllOptions;
		
		@FindBy(xpath = "//button[@class='nds-button--outline nds-button dropdown__button fruits']") 
		WebElement ChooseComplexDD;

		@FindBy(xpath="//div[@id = 'root'] //form[1] //button[@class = 'nds-button--solid nds-button'] ")
		WebElement SubmitButton;
		
		@FindBy(xpath="//button//span/span")
		List<WebElement> FruitList;
		
		@FindBy(xpath="//input[@id ='Disabled']")
		WebElement Clickbox;
		
		@FindBy(className = "css-1g7srr6")
		WebElement ChangeButtonWidth;
		
		@FindBy(id="tabbutton-actions-1")
		WebElement Action;
		
		@FindBy(xpath="//span[normalize-space()='onSubmit']")
		WebElement OnSubmit;
		
		@FindBy(xpath="//span/span[3]")
		WebElement Confirmation;
		
		@FindBy(className="css-13zuhto")
		WebElement SortingButton;
		
		@FindBy(xpath="//select/option")
		List<WebElement> SortingOptions;
		
		@FindBy(xpath = "//*[@id=\"panel-tab-content\"]/div[2]/button[2]")
		WebElement ResetButton;
		
		@FindBy(xpath = "//button[@class = 'tabbutton tabbutton-active css-17imonf']")
		WebElement Knob;
		
		@FindBy(id="dropdown--flipping-placement")
		WebElement FlippingOption;
		
		
		//option[@value="Ascending"]
		public WebElement DefaultDropDown()
		{
			return DefaultDropDown;
		}

		public WebElement Selectdropdown()
		{
			return mainDropdown;
		}
		
		public WebElement MatchListBoxWidth()
		{
			return MatchListBoxWidth;
		}
		
		public WebElement MatchButtonWidth()
		{
			return MatchButtonWidth;
		}
		
		public WebElement AdjustListWidth()
		{
			return ListboxWidth;
			
		}
			
		public WebElement FullyControlled()
		{
			return FullyControlled;
		}
		
		
		public WebElement ComplexOption()
		{
			return ComplexOption;
		}
		
		public WebElement DifferentChildrenType()
		{
			return DifferentChildrenType;
		}
		
		public WebElement FlippingOption()
		{
			return FlippingOption;
			
		}
		
		public List<WebElement> dropDown_ListAllOptions()
		{
			return dropDown_ListAllOptions;
		}
		
		public List<WebElement> FruitList()
		{
			return FruitList;
		}
		
		public WebElement PageFrame()
		{
			return Frame;
		}
		

		
		public WebElement ChooseElement() //Elements present in the dropdown
		{
			return ChooseElement;
		}
		

		
		public WebElement ChooseComplexDD() //Elements present in the dropdown for compelx element - Defined with Select clause 
		{
			return ChooseComplexDD;
			
		}
		

		public WebElement SubmitButton()
		{
			return SubmitButton;
		}
		
		public WebElement Action()
		{
			return Action;
		}
		
		public WebElement OnSubmit()
		{
			return OnSubmit;
		}
		
		public WebElement Confirmation()
		{
			return Confirmation;
		}
		
		public WebElement ClickBox()
		{
			return Clickbox;
		}
		
		public WebElement ChangeButtonWidth()
		{
			return ChangeButtonWidth;
		}
		
		public WebElement SortingButton()
		{
			return SortingButton;
		}
		
		public List<WebElement> SortingOptions()
		{
			return SortingOptions;
		}
		


		public WebElement ResetButton()
		{
			return ResetButton;
		}
		
		public WebElement Knob()
		{
			return Knob;
		}
		

	}

		
		
		
		







