package wwnorton;

	import java.util.Iterator;
	import java.util.List;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.interactions.Actions;
	import org.testng.annotations.*;
	import org.testng.annotations.Test;

	import wwnorton_objectfactory.Component_Dropdown;
	import wwnorton_utilities.DriverInitializer;
	//import jdk.internal.misc.FileSystemOption;

	public class VerifyDropDownsTests {

		WebDriver driver = DriverInitializer.driver;
		Component_Dropdown ob;
		
		
		@BeforeClass
		public void runOnceAtStart() throws InterruptedException
		{
			driver.manage().window().maximize();
			ob = new Component_Dropdown(driver);
			ob.Selectdropdown().click();
			Thread.sleep(200);
		}
		
		//Use @AfterSuite and figure out where this has to be placed in case of multi files run
		@AfterClass
		public void runOnceAtEnd()
		{
			driver.close();
		}
		

		 @Test
		 public void adefaultDropDown() throws InterruptedException
		 {
			 System.out.println("DEFAULT DROPDOWN");
			 driver.switchTo().defaultContent();
			 ob.DefaultDropDown().click();
			 Thread.sleep(200);
			 
			 driver.switchTo().frame(ob.PageFrame());
			 ob.ChooseElement().click();
			 Thread.sleep(200);
			 
			 List<WebElement> allOptions = ob.dropDown_ListAllOptions();
				
			 WebElement x = null;
			 System.out.print("List of options available");
				
				Iterator<WebElement> itr = allOptions.iterator();
				
				while (itr.hasNext()) {
					WebElement found = itr.next();
					if (found.getText().equals("Americium")) {
						x = found;
					}
					
					System.out.println(found.getText());
				}
				x.click();
				
			   System.out.println(""+"Default options: Dimensions of the dropdown:");
			   System.out.println("Height is: " +ob.ChooseElement().getRect().getDimension().getHeight());
			   System.out.println("Width is: "+ ob.ChooseElement().getRect().getDimension().getWidth());
			   
			   
			   driver.switchTo().defaultContent();
			   ob.SortingButton().click();
			   List<WebElement> allSortingOptions = ob.SortingOptions();	 
			  
	 
			  WebElement y = null;
			  Iterator<WebElement> itr1 = allSortingOptions.iterator();
			  
			  while(itr1.hasNext()) {
				  WebElement foundvalue = itr1.next();
				  	
				  	if(foundvalue.getText().equals("Ascending")) 
				  		{
				  		 y = foundvalue;
				  	    }
				 
			       }
				  y.click();
				  Thread.sleep(200);
				  
		
				   ob.ClickBox().click();
				   Thread.sleep(200);
				   ob.ClickBox().click();
				   System.out.println('\n');
		 }
		

		 @Test
		public void bmatchlistBoxWidth() throws InterruptedException {
			
			
			System.out.println("MatchListWidth");
			
			driver.switchTo().defaultContent();
			
			ob.MatchListBoxWidth().click();
			Thread.sleep(200);
			driver.switchTo().frame(ob.PageFrame());
			
			ob.ChooseElement().click();
			Thread.sleep(200);

			List<WebElement> allOptions = ob.dropDown_ListAllOptions();
			
			System.out.println("Number of options present " +allOptions.size());
			

			Iterator<WebElement> itr = allOptions.iterator();
			while (itr.hasNext()) {
				itr.next().getText();
			}
			
			driver.switchTo().defaultContent();
			ob.Knob().click();
			Thread.sleep(200);
			ob.ClickBox().click();
			Thread.sleep(200);
			System.out.println('\n');		
			
		}

		@Test
		public void fullyControlled() throws InterruptedException {

			System.out.println("FULLYCONTROLLED");
			driver.switchTo().defaultContent();
			ob.FullyControlled().click();
			
			Thread.sleep(200);
			driver.switchTo().frame(ob.PageFrame());

			ob.ChooseElement().click();
			Thread.sleep(200);
			
			List<WebElement> allOptions = ob.dropDown_ListAllOptions();
			
			System.out.println("No.of options: "+allOptions.size());

			WebElement x = null;
			Iterator<WebElement> itr = allOptions.iterator();
			while (itr.hasNext()) {
				WebElement found = itr.next();
				if (found.getText().equals("Plutonium")) {
					x = found;
				}
			}
			
			x.click();
			Thread.sleep(200);

			ob.SubmitButton().submit();
			Thread.sleep(200);
			
			System.out.println("Alert Message: "+driver.switchTo().alert().getText());
			
			driver.switchTo().alert().dismiss();
			Thread.sleep(200);
			
			driver.switchTo().defaultContent();
		
			ob.Action().click();
			Thread.sleep(200);
		
			System.out.println("Text displayed when click on Action");
			System.out.println(ob.OnSubmit().getText());
			System.out.println(ob.Confirmation().getText());
			
		}


		@Test
		public void dcomplexOption() throws InterruptedException {
			
			System.out.println("COMPLEX OPTIONS");
			driver.switchTo().defaultContent();
			
			
			ob.ComplexOption().click();
			Thread.sleep(200);
			
			driver.switchTo().frame(ob.PageFrame());
			ob.ChooseComplexDD().click();
			Thread.sleep(200); 
			System.out.println("ComplexOptions:  Dimensions before change");
			System.out.println(ob.ChooseComplexDD().getRect().getDimension().getHeight());
			System.out.println(ob.ChooseComplexDD().getRect().getDimension().getWidth());
			
			driver.switchTo().defaultContent();
			ob.ChangeButtonWidth().clear();
			Thread.sleep(200);
			ob.ChangeButtonWidth().sendKeys("25");
			Thread.sleep(200);
			
			driver.switchTo().frame(ob.PageFrame());
			System.out.println("Complex Options:  Dimensions after change");
			System.out.println(ob.ChooseComplexDD().getRect().getDimension().getHeight());
			System.out.println(ob.ChooseComplexDD().getRect().getDimension().getWidth());
			
//			List<WebElement> chooseFruit = ob.FruitList();
//			WebElement i = null;
//			Iterator<WebElement> itr = chooseFruit.iterator();
//			while(itr.hasNext()) {
//				WebElement j = itr.next();
//				if (j.getText().equals("orange")) {
//					i = j;
//					}
//				System.out.println(j.getText());
//				}
//				
//			i.click();
			

			driver.switchTo().defaultContent();
			
			ob.SortingButton().click();
			Thread.sleep(200);

			List<WebElement> allSortingOptions = ob.SortingOptions();	 
			  
			  WebElement y = null;
			  Iterator<WebElement> itr1 = allSortingOptions.iterator();
			  
			  while(itr1.hasNext()) {
				  WebElement foundvalue = itr1.next();
				  	
				  	if(foundvalue.getText().equals("Descending")) 
				  		{
				  		 y = foundvalue;
				  	    }
			       }
			  Thread.sleep(200);
			  y.click();
			  Thread.sleep(200);

			ob.ClickBox().click();
			Thread.sleep(200);
			
			ob.ClickBox().click();
			Thread.sleep(200);
			
			ob.ResetButton().click();
			Thread.sleep(200);
			System.out.println("");
		}	

		@Test
		public void ediffChildren() throws InterruptedException {
			
			System.out.println("DIFFERENT CHILDREN OPTION");
			driver.switchTo().defaultContent();
			
			ob.DifferentChildrenType().click();
			Thread.sleep(200);
			
			driver.switchTo().frame(ob.PageFrame());

			ob.ChooseElement().click();
			Thread.sleep(200);
			
			List<WebElement> AllOptions = ob.dropDown_ListAllOptions();
			WebElement x = null;
			System.out.println("List of options:");
			Iterator<WebElement> itr = AllOptions.iterator();
			while (itr.hasNext()) {
				WebElement found = itr.next();
				if (found.getText().equals("Bar")) {
					x = found;
				}
				
				System.out.println(found.getText());
			}
			
			x.click();
			Thread.sleep(200);
			System.out.println(" ");
		}
		
		
		@Test	
		public void cmatchbuttonWidth() throws InterruptedException
		{
			System.out.println("MatchButtonWidth");
			driver.switchTo().defaultContent();
			ob.MatchButtonWidth().click();
			Thread.sleep(200);
			driver.switchTo().frame(ob.PageFrame());
			ob.ChooseElement().click();
			Thread.sleep(200);
			
			List<WebElement> allOptions =  ob.dropDown_ListAllOptions();
			System.out.println("Number of options present in Match Button Width List: "+allOptions.size());
			
			WebElement x=null;
			Iterator<WebElement> itr = allOptions.iterator();
			
			while (itr.hasNext()) {
				WebElement y =itr.next();
					if(y.getText().equals("Fermium")) {
						x = y;
					}
			}
				x.click();
				Thread.sleep(200);
				
				System.out.println("MatchButtonWidth: Dimensions before changing the button size");
				System.out.println(ob.ChooseElement().getRect().getDimension().getHeight());
				System.out.println(ob.ChooseElement().getRect().getDimension().getWidth());
				
				driver.switchTo().defaultContent();
				ob.Knob().click();
				Thread.sleep(200);
				ob.ChangeButtonWidth().clear();
				Thread.sleep(200);
				ob.ChangeButtonWidth().sendKeys("20");
				Thread.sleep(200);
				
				driver.switchTo().frame(ob.PageFrame());
				System.out.println("Dimensions after changing the button size");
				System.out.println(ob.ChooseElement().getRect().getDimension().getHeight());
				System.out.println(ob.ChooseElement().getRect().getDimension().getWidth());
				
				driver.switchTo().defaultContent();
				ob.Knob().click();
				Thread.sleep(200);
				ob.ClickBox().click();
				Thread.sleep(200);
				System.out.println(" ");
				}
		
//		@Test
//		public void flippingOptions() throws InterruptedException {
//			
//			driver.switchTo().defaultContent();
//			ob.FlippingOption().click();
//			
//			driver.switchTo().frame(ob.PageFrame());
	//	
//			
//			ob.ChooseElement().click();
//			
//			Actions act = new Actions(driver);
//			act.sendKeys(Keys.PAGE_DOWN).build().perform();
//			 System.out.println("Scroll down performed");
//			Thread.sleep(200);
//			//ob.ChooseElement().click();
//			
//			act.sendKeys(Keys.PAGE_UP).build().perform();      
//	        System.out.println("Scroll up performed");
//	        Thread.sleep(200);
	//	
	//	
//		}
	//	
		

	}
		
		
		
		

			
			

			



	
	


